extern crate ethkey;
extern crate ethstore;
extern crate bitcoin;
extern crate rustc_serialize;
extern crate secp256k1;
extern crate ethcore_bigint;
extern crate crypto as rcrypto;
extern crate bip39;
extern crate rand;

use std::fmt;
use std::str::FromStr;
use rustc_serialize::hex::{ToHex};

use ethcore_bigint::hash::{H512};
use secp256k1::Secp256k1;
use bitcoin::util::bip32::{ExtendedPrivKey, ExtendedPubKey, ChildNumber};
use bitcoin::network::constants::Network;
use bip39::{Language, Mnemonic, Seed};
use ethkey::{ExtendedKeyPair, Derivation};
use ethstore::{EthStore, SimpleSecretStore, SecretVaultRef, IndexDerivation, Derivation as StoreDerivation};

mod util;

use util::TransientDir;

pub type Address = String;
pub type PrivateKey = String;
pub type PublicKey = String;

pub enum Key {
    Private(PrivateKey),
    Public(PublicKey),
}

pub struct U32Derivation(Derivation<u32>);

impl U32Derivation {
    fn child_number(&self) -> ChildNumber {
        match self.0 {
            Derivation::Hard(value) => ChildNumber::Hardened(value - 0x80000000),
            Derivation::Soft(value) => ChildNumber::Normal(value),
        }
    }

    fn derivation(&self) -> Derivation<u32> {
        match self.0 {
            Derivation::Hard(value) => Derivation::Hard(value),
            Derivation::Soft(value) => Derivation::Soft(value),
        }
    }

    fn index_derivation(&self) -> IndexDerivation {
        match self.0 {
            Derivation::Hard(value) => IndexDerivation {
                soft: false,
                index: value,
            },
            Derivation::Soft(value) => IndexDerivation {
                soft: true,
                index: value,
            },
        }
    }
}

impl fmt::Debug for U32Derivation {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self.0 {
            Derivation::Hard(value) => write!(f, "Hard({})", value),
            Derivation::Soft(value) => write!(f, "Soft({})", value),
        }
    }
}

pub fn parse_derivation_path(path: &str) -> Vec<U32Derivation> {
    if !path.starts_with('m') {
        panic!("path must be starts with m");
    }
    let mut result: Vec<U32Derivation> = vec![];
    let parts: Vec<&str> = path.split("/").collect();

    for part in parts {
        if part == "m" {
            continue;
        }
        if part.ends_with('\'') {
            let num = part[..part.len() - 1].trim().parse::<u32>().ok().expect(
                "invalid Hard number",
            );
            result.push(U32Derivation(Derivation::Hard(0x80000000 + num)));
        } else {
            let num = part.trim().parse::<u32>().ok().expect(
                "invalid Soft number",
            );
            result.push(U32Derivation(Derivation::Soft(num)));
        }
    }
    result
}


pub fn seed_from_mnemonic(phrase: &str, password: &str) -> Seed {
    let mnemonic = Mnemonic::from_string(phrase, Language::English, password).unwrap();
    mnemonic.as_seed().clone()
}


pub fn bitcoin_derive(seed: &[u8], paths: &[U32Derivation]) -> (PublicKey, PrivateKey) {
    let secp = &Secp256k1::new();
    let network = Network::Bitcoin;
    let mut sk = ExtendedPrivKey::new_master(secp, network, seed).unwrap();
    println!("root secret: {:?}", sk.secret_key);
    let mut pk = ExtendedPubKey::from_private(secp, &sk);
    // Derive keys, checking hardened and non-hardened derivation
    for path in paths.iter() {
        let num = path.child_number();
        sk = sk.ckd_priv(secp, num).unwrap();
        match num {
            ChildNumber::Normal(_) => {
                let pk2 = pk.ckd_pub(secp, num).unwrap();
                pk = ExtendedPubKey::from_private(secp, &sk);
                assert_eq!(pk, pk2);
            }
            ChildNumber::Hardened(_) => {
                pk = ExtendedPubKey::from_private(secp, &sk);
            }
        }
    }
    let strs: Vec<String> = sk.secret_key[..]
        .to_vec()
        .iter()
        .map(|b| format!("{:02x}", b))
        .collect();
    (
        pk.public_key.serialize_vec(secp, false).to_hex(),
        strs.join(""),
    )
}

pub fn parity_derive(seed: &[u8], paths: &[U32Derivation]) -> (PublicKey, PrivateKey) {
    let mut extended = ExtendedKeyPair::with_seed(seed).unwrap();
    println!("root secret: {:?}", extended.secret().as_raw());
    for item in paths {
        extended = extended.derive(item.derivation()).unwrap();
    }
    (
        extended.public().public().hex(),
        String::from(&*extended.secret().as_raw().hex()),
    )
}


fn store_derive(seed: &[u8], paths: &[U32Derivation]) -> Address {
    let secret_key = ExtendedKeyPair::with_seed(seed).unwrap();
    println!("root secret: {:?}", secret_key.secret().as_raw());

    let dir = TransientDir::create().unwrap();
    let store = EthStore::open(Box::new(dir)).unwrap();

    let sref = store
        .insert_account(SecretVaultRef::Root, secret_key.secret().as_raw().clone(), "")
        .unwrap();

    let mut new_paths: Vec<IndexDerivation> = vec![];
    for item in paths {
        let index_derivation = item.index_derivation();
        println!("index derivation: {}, {}", if index_derivation.soft { "Soft" } else { "Hard" }, index_derivation.index);
        new_paths.push(index_derivation);
    }

    let derived = store
        .insert_derived(
            SecretVaultRef::Root,
            &sref,
            "",
            StoreDerivation::Hierarchical(new_paths),
        )
        .unwrap();

    // println!("derived secret: {:?}", store.raw_secret(&derived, "").unwrap().into());
    // remove root account
    store.remove_account(&sref, "").unwrap();
    derived.address.hex()
}

fn main() {
    let phrase = "lemon voice require wisdom rain soft allow sing mule flee fox scare";
    // let seed: &str = "1a42e1fa51c447c16c6f84e8c8dcfdc10f7474d75e6d4773d24fd3c76f7630f796a2cd15763c5ff16269a2753d57caf95b59a7f218ae306142e8f0a02001e96a";
    let password = "";
    let path = "m/44'/60'/0'/0/0";
    let seed = seed_from_mnemonic(phrase, password);
    let paths = parse_derivation_path(path);
    println!(
        "\nphrase: {}\nseed: {}\npassword: {}\npath: {}\nparsed paths: {:?}",
        phrase,
        seed.as_hex().to_lowercase(),
        password,
        path,
        &parse_derivation_path(path)
    );

    println!("\nRust-bitcoin bip32\n------------------------");
    let (public_key, private_key) = bitcoin_derive(seed.as_bytes(), &paths);
    println!(
        "public_key: {}\nprivate key: {}",
        public_key,
        private_key
    );

    println!("\nParity extended\n------------------------");
    let (public_key, private_key) = parity_derive(seed.as_bytes(), &paths);
    println!(
        "public_key: {}\naddress: {}\nprivate key: {}",
        public_key,
        ethkey::public_to_address(&H512::from_str(public_key.as_str()).unwrap()),
        private_key
    );

    println!("\nEthStore insert_derived\n------------------------");
    let derived_address = store_derive(seed.as_bytes(), &paths);
    println!("derived address: {}", derived_address);
}
